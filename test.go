package main

import(
	//math "MySource/MyPackages/math"
	"fmt"
)

type Shape interface {
	Area() float64

}

type Rectangle struct {
	width float64
	height float64
}

func (r Rectangle) Area() float64{
	return r.width + r.height;
}


func main() {
	var r Shape
	r = Rectangle{ width:3, height:5}
	fmt.Println(r.Area())
}
